module timecounter(
	input global_clk,
	input start,
	input clk,
    input reset,
    output reg enable,
    output [15:0] t
    );
    wire next_count,unused;
    wire[3:0] count1,count2 ;
    reg[1:0] state;
    reg [4:0] internal_time; 
    reg [3:0] countdown_time; 
    reg resets1;

    BCD_Counter_4bit b0 (.clock(clk),.reset(enable),.next_count(next_count),.bcd_count(count1)); 
	BCD_Counter_4bit b1 (.clock(next_count),.reset(enable),.next_count(unused),.bcd_count(t[7:4])); 
	countdown d0(.clk(clk),.reset(resets1), .out(count2)); 
    always @(posedge global_clk) begin 
		if(reset==0) 
			state<=0;
		else begin
			if(state==0 && start==1) begin 
				state<=1;
			end else if  (state==1 && t==0) begin 
				state<=2; 
			end else if(state==2 && t==8'h30) begin 
				state<=3;
			end else if  (state==3 && t==0) begin 
				state<=0;
			end
		end // if
	end // always
        
	always@(*)begin 
		if(state==0) begin 
			resets1=0;  
			enable=0;    
		end else if(state==1) begin 
			resets1=1;
			enable=0; 
		end else if(state==2) begin 
			resets1=0;
			enable=1;
		end else if(state==3) begin 
			resets1=1;
			enable=0;
		end  
	end    
        
	assign t[3:0]= (resets1==1)? count2: count1; 
endmodule


module countdown(input clk, input reset, output reg [3:0]  out); 

always @(posedge clk) begin 
if(reset==0 || out==0) begin 
out<=5;
end 
else 
out<=out-1; 
end 

endmodule 
