module BCD_Counter_16bit(
	input clock,
	input reset,
	output [15:0] bcd_count
	);

	wire [3:0] carry;


BCD_Counter_4bit BCD_Counter_4bit1(
	.clock			(clock),
	.reset			(reset),
	.bcd_count      (bcd_count[3:0]),
	.next_count     (carry[0])
	);

BCD_Counter_4bit BCD_Counter_4bit2(
	.clock			(carry[0]),
	.reset			(reset),
	.bcd_count      (bcd_count[7:4]),
	.next_count     (carry[1])
	);

BCD_Counter_4bit BCD_Counter_4bit3(
	.clock			(carry[1]),
	.reset			(reset),
	.bcd_count      (bcd_count[11:8]),
	.next_count     (carry[2])
	);

BCD_Counter_4bit BCD_Counter_4bit4(
	.clock			(carry[2]),
	.reset			(reset),
	.bcd_count      (bcd_count[15:12]),
	.next_count     (carry[3])
	);

endmodule