// This is an implementation of a 4 bit linear feedback
// shift register used to generate random numbers in a
// Moore machine model
//
// source: https://www.eetimes.com/tutorial-linear-feedback-shift-registers-lfsrs-part-1/#

module rand_lfsr(
	input            increment_i,
	output reg [2:0]       bit_o
);
	// registers for LFSR
	reg [9:0] state,
		state_next;

	// registers for 3 bit output
	reg [2:0] bit_int;

	// register to check for invalid state
	reg reset_int;

	// reset value
	parameter reset_val = 10'b1000000000;

	initial
		state = 10'd0;

	// sequential logic
	always @(posedge increment_i) begin
		state <= (!reset_int) ? reset_val : state_next;
		bit_o <= state_next[2:0];
	end

	// combinational logic
	always @(*) begin
		  bit_int[2] = state[9] ^ state[2];
		  bit_int[1] = state[8] ^ state[1];
		  bit_int[0] = state[7] ^ state[0];
		state_next   = {state[6:0] , bit_int};
		reset_int    = |state;
	end
endmodule
