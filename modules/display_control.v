`include "seven_segment_decoder.v"

module display_control(
	input                    clock_i,
	input      [15:0]        score_i,
	input      [15:0]         time_i,
	output     [ 6:0]         disp_o,
	output reg [ 7:0] digit_select_o);

	reg [2:0] count_ns_int;
	reg [2:0] count_cs_int;
	reg [3:0]  count_o_int;
	reg [3:0]    count_o;
	
	parameter select_0 = ~(8'b00000001);
	parameter select_1 = ~(8'b00000010);
	parameter select_2 = ~(8'b00000100);
	parameter select_3 = ~(8'b00001000);
	parameter select_4 = ~(8'b00010000);
	parameter select_5 = ~(8'b00100000);
	parameter select_6 = ~(8'b01000000);
	parameter select_7 = ~(8'b10000000);

	seven_segment_decoder display_out (
		.hex_num_i ( count_o ),
		 .decode_o (  disp_o )
	);

	// 3 bit counter
	always @(posedge clock_i)
		count_cs_int <= count_ns_int;
	
	// combinational logic
	always @(*) begin
		count_ns_int <= count_cs_int + 1;
		// multiplexer
		case(count_cs_int)
			3'b000: begin
				digit_select_o = select_0;
				count_o        = score_i[3:0];
			end
			3'b001: begin
				digit_select_o = select_1;
				count_o        = score_i[7:4];
			end
			3'b010: begin
				digit_select_o = select_2;
				count_o        = score_i[11:8];
			end
			3'b011: begin
				digit_select_o = select_3;
				count_o        = score_i[15:12];
			end
			3'b100: begin
				digit_select_o = select_4;
				count_o        = time_i[3:0];
			end
			3'b101: begin
				digit_select_o = select_5;
				count_o        = time_i[7:4];
			end
			3'b110: begin
				digit_select_o = select_6;
				count_o        = time_i[11:8];
			end
			3'b111: begin
				digit_select_o = select_7;
				count_o        = time_i[15:12];
			end
		endcase
	end
endmodule
