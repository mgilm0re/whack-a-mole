module T_FlipFlop(
    input clock, reset, T,
    output reg Q
    );
    
    always @ (posedge clock, negedge reset) begin
        Q <= (!reset) ? 1'b0 : Q ^ T;
    end
endmodule
