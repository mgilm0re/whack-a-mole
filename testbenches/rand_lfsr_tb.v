// test bench for rand_lfsr random number generator

`include "rand_lfsr.v"

module rand_lfsr_tb;

	reg        increment_i;
	reg  [7:0]    count_int;
	wire [2:0]       bit_o;

	parameter MAX = 100;

	rand_lfsr tb (
		.increment_i ( increment_i ),
		      .bit_o (       bit_o )
	);

	initial begin
		increment_i   = 0;
		    count_int = 0;
		#10
		increment_i   = !increment_i;
		    count_int = count_int + 1;
	end

	always begin
		$display("%d",bit_o);
		#5
		increment_i = !increment_i;
		#5
		increment_i   = !increment_i;
		    count_int = count_int + 1;
		if (count_int == MAX)
			$finish;
	end
endmodule
